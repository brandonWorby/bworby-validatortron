<?php
class Validator
{

	private $valid = false;
	private $missing_parameters = array();
	private $valid_parameters = array();
	private $requestMethod = null;
	private $input_data = null;

	public function __construct($userRequestMethod = true ) {
		try {
			if ($userRequestMethod) {
				$this->requestMethod  = $_SERVER['REQUEST_METHOD'];        
			}      
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}

	public static function getInstance() {
		try {
			return new self();
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}

	/**
		setParameters - takes in the definitions for the properties it should pull from the request - this includes the types, required, and other definition properties. 
		this is an example of a param
		'a_boolean'=>[
            'required'=>true,
			'type'=>'boolean',
		]		
		and then validates them. 
		if $throw_exception is false, it will just set the valid property on the validator for checking later from caller. 
	*/
	public function setParameters( $parameters , $throw_exception = true ) {
		 
		if( $this->isPostOrPut() ) {
			$this->setInputdata();
		}
	
		$this->validateParameters($parameters);	
		if (count($this->missing_parameters)) {
			$this->valid = false;
			if ($throw_exception) {
				throw new \Exception($this->getInvalidMessage(), 10001);
			}
		} else {
			$this->valid = true;
		}
	}
	
	/**
		setInputData 
		will check the input data, and if accessable, will set the property input_data
	*/
	private function setInputData()	{
		try {
			$data = file_get_contents("php://input");
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
		if( !is_string( $data ) ) {
			throw new \Exception(" input data not string. ");
		}
		$this->input_data = json_decode($data);
		if( json_last_error() ) {
			$error = $this->getErrorFromJsonError(json_last_error());
			throw new \Exception('Error with input JSON - ' . $error['message'], $error['code']);
		}
	}
	
	/**
		validateParameters
		this will check the passed in param definitions, and then validate based on the rules 
	*/
	private function validateParameters( $parameters ) {
	
		foreach( $parameters as $key => $definition) {
		
			$value = $this->getInputParameterValueFrom( $key , $definition );
		
			if (array_key_exists("valid_values", $definition)) {
				if (!in_array( $value , $definition["valid_values"] ) ) {
					throw new \Exception("Value does not match valid values. $key can only be one of the following: " . join(",", $definition["valid_values"]), 10047);
				}
			}
		
			$this->valid_parameters[$key] = $definition;
			$this->valid_parameters[$key]['value'] = $value;
					
		}
	
	}

	//check if a particular key was set in input
	private function inputDataKeySet( $key ) {
		if( $this->isPostOrPut() ) {
			if( property_exists( $this->input_data , $key ) ) {
				return true;
			}
		} else { //get
			$req_val = $this->getGetParamForKey( $key );
			if (isset($req_val)) {
				return true;
			}
		}
		return false;
	}
		
	private function getGetParamForKey( $key ) {
		if( isset( $_REQUEST[$key] ) ) {
			return $_REQUEST[$key];
		}
		return null;
	}

	public function getInputDataForKey($key) {
		try {
			return $this->input_data->$key;
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}
  
  
	/**
	* @param $key - the key from the params from route
	* @param $definition - the extra array data from def in route for key
	* @param null $value - if passed in, we will use it for validation, not check the input PUT / POST data || GET param
	* @return array|mixed|null - returns the value we calcualated / created from the definition rules and input data / value
	* @throws \Exception
	*/
	private function getInputParameterValueFrom($key, $definition, $value = null)
	{
	
		$val = null;
		if (!is_null($value)) {
			$val = $value;
		} else {
			//otherwise look for value in request
			//get value from input
			if ($this->isPostOrPut()) {
				if (property_exists($this->input_data, $key)) {
					if (isset($this->input_data->$key)) {
						$val = $this->input_data->$key;
					} else {
						//null -
						$val = null;
					}
				} else {
					//check for default - then required,
					if (  array_key_exists( "default" , $definition ) ) {
						$val = $definition['default'];
					} else {
						//if required not set, or is set to 0 .. continue on and skip this bugger
						if (!isset($definition['required'])) {
							//continue;
						} else if ($definition['required'] == 0) {
							//continue;
						} else {
							//null and should not be allowed to be. let it fall through to null checks / error throwing below
						}
					}
				}
			
			} else {
				$req_val =  $this->getGetParamForKey($key);			
				$val = !is_null($req_val) ? $req_val : $val;
				if (is_null($val)) {
					if (isset($definition['default'])) {
						$val = $definition['default'];
					} else {
						//continue;
					}
				} else {
				//was set -
				}
			}
		}
		
		if( !isset( $definition['type'] ) ) {
	      throw new \Exception("Error - type was not set for property :$key ");
		}
		
		//if value
		if( is_null($val) ) {
				
			//if is NULL, allow null is false, and property was passed as null - throw error --
			//if null, and allow_null is false, but not passed its ok, unless required. handled below
			if( isset( $definition['allow_null'] ) && !$definition['allow_null'] ) { //if allow null not set or false - this is an error.
				throw new \Exception("Null not allowed for key: $key", 10053);
			} else {
				if( isset( $definition['allow_null'] ) && $definition['allow_null'] ) {
					return null;
				}
			}
		
			//if the parameters had required set
			if (!empty($definition['required'])) {
				//and its true
				if ($definition['required']) {
					//add to missing parameters
					array_push($this->missing_parameters, $key);
				}
			}
			return null;
		}
		
		switch ($definition['type']) {
	
			case "associative_array":
			case "array":
				return $this->handleArray($definition, $val, $key);
				break;
			case "array_flat":
				$tmp_array = [];
				if( !is_array( $val )  ) {
					throw new \Exception("Not an array for type array_flat key: $key.", 5656565656);
				}
				foreach( $val as $single_flat_array_element ) {
					array_push( $tmp_array , forceType($single_flat_array_element, $definition['array_flat_data_type'] ) );
				}
				return $tmp_array;
				break;
			case "datetime":
				if (strlen($val) > 0) {
					if( $this->isTimestampIsoStringValid( $val ) ) {
						return $val;
					} else {
						throw new \Exception("datetime requires format matching yyyy-mm-ddThh:mm:ss Z or +/-dd:dd, [$key].", 10042);
					}
				}
				break;
			case "date":
				if (strlen($val) > 0) {
					if ($this->isDateString($val)) {
						return $val;
					} else {
						throw new \Exception("date requires format matching yyyy-mm-dd, [$key].", 10043);
					}
				}
				break;
			case "email":
				if ($val == "") {
					throw new \Exception("Email can not be empty. ", 34206);
				}
				$trimmed_email = trim($val);
				if (!isValidEmail($trimmed_email)) {
					throw new \Exception("Email is not valid: $trimmed_email ", 34207);
				}
				if (isset($definition['max_length'])) {
					if (strlen($trimmed_email) > $definition['max_length']) {
						throw new \Exception("Max length exceeded: $key. max length:" . $definition['max_length'], 10051);
					}
				}
				return forceType($trimmed_email, "string");
				break;
			case "float":
			case "integer":
			case "int":
				return forceType($val, $definition['type']);				
				break;
			case "list":
				if (is_string($val)) {
					if ($val == "") {
						return [];
					} else {
						if( !isset( $definition['list_data_type'] ) ) {
							throw new \Exception("Property: $key was defined as list. List requires the list_data_type being passed in as well.", 2358 );
						}
						$list_as_array = explode(",", $val);
						for ($i = 0; $i < count($list_as_array); $i++) {
							$list_as_array[$i] = forceType($list_as_array[$i], $definition['list_data_type']);
						}
						if ($list_as_array[0] == null) {
							return [];
						} else {
							return $list_as_array;
						}
					}
				} else {
					throw new \Exception("Value is not a string. $key::$val", 10045);
				}
				break;
			case "json_object":
			case "json-object":
				$decoded = $this->getValidDecodedJSONForKey( $val , $key );
				if( !is_object( $decoded ) ) {
					throw new \Exception('Error with input JSON. JSON object type was not an object. - [' .$key.'] ' , 10066  );
				}
				return $val;
				break;
			case "json_array":
			case "json-array":
				$decoded = $this->getValidDecodedJSONForKey( $val , $key );
				if( !is_array( $decoded ) ) {
					throw new \Exception('Error with input JSON. JSON array type was not an array. - [' .$key.'] ' , 10067  );
				}
				return $val;
				break;
			case "json":
				$this->getValidDecodedJSONForKey( $val , $key );
				return $val;
				break;
			case "object":
				return $this->handleObject($definition, $val, $key);
				break;
			case "string":
				return $this->handleString($key, $definition, $val);
				break;    
			case "boolean":
			case "bool":
			case 'bool_as_int':
			case 'boolean_as_int':
				return forceType( $val , $definition['type'] );
				break;  
			case "time":
				if (strlen($val) > 0) {
					if ($this->isTimeString($val)) {
						return $val;
					} else {
						throw new \Exception("time requires format matching hh:mm:ss, [$key].", 10044);
					}
				}
				break;
			case "username":
				if ($val == "") {
					throw new \Exception("Username can not be empty. ", 34204);
				} else {
					$restricted_names = ['get', 'create', 'edit', 'delete'];
					if (in_array($val, $restricted_names)) {
						throw new \Exception("Username is not valid: $val ", 34205);
					}
				}
				$trimmed_username = trim($val);
				if (preg_match('/^[a-z0-9\&\*\+\-\=\?\"\^\(\)\_\/\{\}\<\>\[\]\'\@\;\,\:\|\#\!\~\`\%\.\-\$]*$/i', $trimmed_username) > 0) {
				} else {
					throw new \Exception("Username is not valid: $trimmed_username ", 34205);
				}
				if (isset($definition['max_length'])) {
					if (strlen($trimmed_username) > $definition['max_length']) {
						throw new \Exception("Max length exceeded: $key. max length:" . $definition['max_length'], 10051);
					}
				}
				return forceType($trimmed_username, "string");
				break;
			default:          
			throw new \Exception("Type not handled. Type: " . $definition['type'] . " for param $key", 10039);
	
		} //end switch
		

	}


	function getValidDecodedJSONForKey( $json_string , $key ){
		if( !is_string( $json_string ) ) {
			throw new \Exception('Error with input JSON - [' .$key.']' . " not a string" );
		}
		$json_decode = json_decode($json_string);
		if( json_last_error() ) {
			$error = $this->getErrorFromJsonError(json_last_error());
			throw new \Exception('Error with input JSON - [' .$key.'] ' . $error['message'], $error['code']);
		}
		return $json_decode;
	}
	
	/**
		handleString
		checks the various rules that can apply with strings. 
	*/
	private function handleString($key, $definition, $string) {
	
		$updated_value = forceType($string, $definition['type']);
		
		if( isset($definition['max_length'] ) ) {
			if (strlen($updated_value) > $definition['max_length']) {
				throw new \Exception("Max length exceeded: $key. max length:" . $definition['max_length'], 10051);
			}
		}
		
		if( isset( $definition['allow_empty_sting_to_null'] ) && $definition['allow_empty_sting_to_null'] ) {
			$trimmed = trim($updated_value);
			if ($trimmed == "") {
				$updated_value = null;
			}
		}
		
		if( isset( $definition['underscore_to_space'] ) && $definition['underscore_to_space'] ) {
			$updated_value = str_ireplace( "_" , " " , $updated_value );
		}


		return $updated_value;	
	}
	
	/**
		handleObject
		this function handles a property type of object. it can have any types inside it, and can be recursive between itself and or handling array. 
	*/
	function handleObject($definition, $object_data, $key = null) {
	
		$definition_properties = [];
		if (isset($definition['properties'])) {
			$definition_properties = $definition['properties'];
		} else if (isset($definition['types'])) {
			$definition_properties = $definition['types'];
		}
		
		if (!count($definition_properties)) {
			throw new \Exception("definition_properties not set", 10057);
		}
		
		$object_return_data = [];
	
		foreach ($definition_properties as $def_key => $def_value) {
			
			$required = isset($def_value['required']) ? $def_value['required'] : false;
			$has_default = isset($def_value['default']) ? true : false;
			$type = $def_value['type'];
			
			if (isset($object_data->$def_key)) {
				if ( $type == "object" ) {
					$object_return_data[$def_key] = $this->handleObject($def_value, $object_data->$def_key, $key);
				} else if ( $type == "array" ) {
					$object_return_data[$def_key] = $this->handleArray($def_value, $object_data->$def_key, $key);
				} else {
					$object_return_data[$def_key] = $this->getInputParameterValueFrom($def_key, $def_value, $object_data->$def_key);
				}
				
			} else {
				if ($has_default) {
					//set default
					$object_return_data[$def_key] = $def_value['default'];
				} else {
					if ($required) {
					throw new \Exception("Value was required but not set $def_key::$type. ", 10041);
					}
				}		
			}
	
		}
	
		return $object_return_data;
	
	}
	
	/**
		handleArray
		this function handles a property type of array. it can have any types inside it, and can be recursive between itself and or handling object. 
	*/	
	function handleArray($definition, $array_data, $key = null) {
		
		$definition_properties = [];
		if (isset($definition['properties'])) {
			$definition_properties = $definition['properties'];
		} else if (isset($definition['types'])) {
			$definition_properties = $definition['types'];
		}
		
		if (!count($definition_properties)) {
			throw new \Exception("definition_properties not set", 10057);
		}
		
		//allow empty is different than required. this would allow required true, but allow empty false.
		$allow_empty_array = isset($definition['allow_empty_array']) ? $definition['allow_empty_array'] : false;
		if (empty($array_data)) {
			if (!$allow_empty_array) {
				throw new \Exception("Empty array not allowed. key: $key", 10061);
			} else {
				return [];
			}
		}
		
		//iterate array 0,1,...
		foreach ($array_data as $key => $item) {
		
			$item_return_data = [];
		
			//if definition is an array of data - LIKE 'types'=>[ 'enabled' => ['type' => 'boolean', 'default' => true ] ,
			if (is_array($definition_properties)) {
				foreach ($definition_properties as $def_key => $def_value) {
					
					$required = isset($def_value['required']) ? $def_value['required'] : false;
					
					//$has_default = isset($def_value['default']) ? true : false;
					$has_default = array_key_exists( "default" , $def_value ) ? true : false;
					
					$allow_empty_array = isset($def_value['allow_empty_array']) ? $def_value['allow_empty_array'] : true;
					$type = $def_value['type'];
					
					
					if (is_array($item) || is_object($item)) {
						if (!array_key_exists($def_key, $item)) {
							//if key does not exist in data passed in
							if ($has_default) {
								//set default
								$item_return_data[$def_key] = $def_value['default'];
							} else {
								if ($required) {
									throw new \Exception("Value was required but not set $def_key::$type. ", 10041);
								}
							}
						} else {
							//key does exist in passed in data -
							//force as array -
							$item_as_array = (array)$item;
					
							if (empty($item_as_array)) {
								if (!$allow_empty_array) {
								throw new \Exception("Empty array not allowed.", 10061);
								}
							}
					
							if ( $type == "array") {
								//re-call self
								$item_return_data[$def_key] = $this->handleArray($def_value, $item_as_array[$def_key], $key);
							} else if ($type == "object") {
								$item_return_data[$def_key] = $this->handleObject($def_value, $item_as_array[$def_key], $key);
							} else {
								//some other type - let the param function handle it -
								$item_return_data[$def_key] = $this->getInputParameterValueFrom($def_key, $def_value, $item_as_array[$def_key]);
							}
						}
					
					} else {
						throw new \Exception("Array item has unknown object type $def_key::$type.", 10058);
					}
					
				}
				
				$return_result[] = $item_return_data;
			
			} else { //single value for types - i.e. "string"
				$return_result = [];
				$typesStringValue = $definition_properties;
				foreach ($array_data as $array_item) {
					if (is_array($array_item)) {
						throw new \Exception("Types value does not work with provided data", 10060);
					}
					array_push($return_result, forceType($array_item, $typesStringValue));
				}
			}
		
		}
		
		return $return_result;
	
	}

	
	/**
		isTimeString
	*/
	public function isTimeString($value) {
		try {
			if (preg_match("/^(\d{2}):(\d{2}):(\d{2})$/", $value)) {
				return true;
			}
			return false;
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}
	
	/**
		isDateString
	*/	
	public function isDateString($value) {
		try {
			if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $value)) {
				return true;
			}
			return false;
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}
	
	/**
		isTimestampIsoStringValid
	*/	
	function isTimestampIsoStringValid($timestamp) {
		try {
			return $this->assertISO8601Date($timestamp);
		} catch (\Exception $e) {
		throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}
	
	/**
		assertISO8601Date
	*/
	function assertISO8601Date($dateStr) {
		if (preg_match('/^\b[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}([zZ]{1}|[\+-]{1}[0-9]{2}:[0-9]{2})\b$/', $dateStr) > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
		getErrorFromJsonError
	*/	
	private function getErrorFromJsonError($jsonError) {
		try {
			$error_data = [];
			switch ($jsonError) {
				case JSON_ERROR_NONE:
					return ['code' => 10030, 'message' => 'No errors'];
					break;
				case JSON_ERROR_DEPTH:
					return ['code' => 10031, 'message' => 'Maximum stack depth exceeded'];
					break;
				case JSON_ERROR_STATE_MISMATCH:
					return ['code' => 10032, 'message' => 'Underflow or the modes mismatch'];
					break;
				case JSON_ERROR_CTRL_CHAR:
					return ['code' => 10033, 'message' => 'Unexpected control character found'];
					break;
				case JSON_ERROR_SYNTAX:
					return ['code' => 10034, 'message' => 'Syntax error, malformed JSON'];
					break;
				case JSON_ERROR_UTF8:
					return ['code' => 10035, 'message' => 'Malformed UTF-8 characters, possibly incorrectly encoded'];
					break;
				default:
					return ['code' => 10036, 'message' => 'Unknown error'];
					break;
			}
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}

	/**
		getValidParameters
	*/	
	public function getValidParameters() {
		return $this->valid_parameters;
	}
	
	/**
		isPostOrPut
		isPostOrPut
	*/	
	public function isPostOrPut() {
		try {
			if( strtolower($this->requestMethod) == 'post' || strtolower($this->requestMethod) == 'put') {
				return true;
			}
			return false;
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}

	/**
		getInvalidMessage
		//returns string with all missing parameters appended.
	*/	
	public function getInvalidMessage() {
		return "One or more required parameters are missing or blank: " . implode(", ", $this->missing_parameters);
	}

	/**
		isValid
		//returns valid property as true or false also could return as string.
	*/	
	public function isValid($as_string = false) {
		try {
			if ($as_string) {
				if ($this->valid) {
					return 'true';
				} else {
					return 'false';
				}
			}
			return $this->valid;
		} catch (\Exception $e) {
			throw new \Exception("(Unhandled) ERROR in [" . __METHOD__ . "][" . $e->getLine() . "]: " . $e->getMessage());
		}
	}


}