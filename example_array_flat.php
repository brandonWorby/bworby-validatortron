<?php
	
	include("helpers.php");
	include("Validator.php");

/*
	example payload 
	
	{
		"some_flat_array":[1,2,3,4] 
	}
	
*/
	
    $params = [
		
		
		'some_flat_array'=>[
		    'required'=>false,
		    'array_flat_data_type'=>'int',
		    'type'=>'array_flat',
		],

    ];
    	
    $validator = new Validator();
    try {
      $validator->setParameters($params);
    } catch (\Exception $e) {
      sendBasicResponseAndDie(false, $e->getMessage(), $e->getCode());
    }    
    
    $validated_parameters = $validator->getValidParameters();
    
    sendResponseAndDie(["success" => true, "localization_code" => 2 , "response" => "Validated parameters returned.", "validated_parameters" => $validated_parameters ]);    
	
?>