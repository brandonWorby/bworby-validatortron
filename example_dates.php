<?php
	
	include("helpers.php");
	include("Validator.php");
	
		
    $params = [

		'a_datetime'=>[
            'required'=>true,
			'type'=>'datetime',
		],
		
		'a_time'=>[
            'required'=>true,
			'type'=>'time',
		],
				
		'a_date'=>[
            'required'=>true,
			'type'=>'date',
		],
			

    ];
    	
    $validator = new Validator();
    try {
      $validator->setParameters($params);
    } catch (\Exception $e) {
      sendBasicResponseAndDie(false, $e->getMessage(), $e->getCode());
    }    
    
    $validated_parameters = $validator->getValidParameters();
    
    sendResponseAndDie(["success" => true, "localization_code" => 123456789 , "response" => "Validated parameters returned.", "validated_parameters" => $validated_parameters ]);    
	
?>