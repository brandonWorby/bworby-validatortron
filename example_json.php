<?php
	
	include("helpers.php");
	include("Validator.php");
	
		
    $params = [

		'json'=>[
            'required'=>false,
			'type'=>'json',
		],
	
	
		'json-object'=>[
            'required'=>false,
			'type'=>'json-object',
		],
	
		'json-array'=>[
            'required'=>false,
			'type'=>'json-array',
		],
		
    
    ];
    	
    $validator = new Validator();
    try {
      $validator->setParameters($params);
    } catch (\Exception $e) {
      sendBasicResponseAndDie(false, $e->getMessage(), $e->getCode());
    }    
    
    $validated_parameters = $validator->getValidParameters();
    
    sendResponseAndDie(["success" => true, "localization_code" => 123456789 , "response" => "Validated parameters returned.", "validated_parameters" => $validated_parameters ]);    
	
?>