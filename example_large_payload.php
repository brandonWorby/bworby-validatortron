<?php
	
	include("helpers.php");
	include("Validator.php");
	
/*
	
	example payload:

{
    "some_string_v_v":"test",
    "some_string":"585",
    "a_datetime":"2012-01-18T11:45:00+01:00",
    "a_time":"18:30:00",
    "a_date":"1812-01-18",
    "a_boolean":true,
    "some_boolean":true,
    "some_flat_array":["d","d",3.58726,4],
    "some_array":[
        {
            "enabled":1,
            "name":"name",
            "id":55,
            "sub_array":[
                {
                    "name":"name",
                    "id":58
                }
            ]
        }
    ],
    "a_list":"1,2,3"
}	
	
	
*/	
	
		
    $params = [

		'some_string_v_v' => [
			'type' => 'string',
			'default' => true,
			'valid_values' => ["test"]
		],   
		
		'some_string' => [
			'type' => 'string',
            'required'=>true,
			'max_length' => 5
		],   
		
		'a_datetime'=>[
            'required'=>true,
			'type'=>'datetime',
		],
		
		'a_time'=>[
            'required'=>true,
			'type'=>'time',
		],
				
		'a_date'=>[
            'required'=>true,
			'type'=>'date',
		],

		'a_boolean'=>[
            'required'=>true,
			'type'=>'boolean',
		],			
		
		'some_boolean' => [
			'type' => 'bool_as_int',
		],
													
		'some_flat_array' => [
            'required'=>false,
            'type'=>'array_flat',
            'value_type'=>'int',
		],	

		'some_array' => [
			'type' => 'associative_array',
			'required' => true,
			'types' => [
				'enabled' => [
					'type' => 'boolean',
					'default' => true
				],
				'enable_on' => [
					'type' => 'datetime'
				],
				'name' => [
					'type' => 'string'
				],
				'id' => [
					'type' => 'integer',
					'required' => true
				],	
				'sub_array' => [
				'required' => true,
					'type' => 'associative_array',
					'types' => [
						'name' => [
							'type' => 'string'
						],
						'id' => [
							'type' => 'integer',
							'required' => true
						]	
					]				
				],								
			]
		],

		'some_boolean_with_default' => [
			'type' => 'boolean',
			'default' => false
		],      
		'some_int' => [
			'type' => 'integer',
			'default' => 5
		],
		'a_list' => [
			"list_data_type"=>"int",
			'type' => 'list',
		]		

    ];
    	
    $validator = new Validator();
    try {
      $validator->setParameters($params);
    } catch (\Exception $e) {
      sendBasicResponseAndDie(false, $e->getMessage(), $e->getCode());
    }    
    
    $validated_parameters = $validator->getValidParameters();
    
    sendResponseAndDie(["success" => true, "localization_code" => 2 , "response" => "Validated parameters returned.", "validated_parameters" => $validated_parameters ]);    
	
	
	/*

should return: 

{
    "success": true,
    "localization_code": 2,
    "response": "Validated parameters returned.",
    "validated_parameters": {
        "some_string_v_v": {
            "type": "string",
            "default": true,
            "valid_values": [
                "test"
            ],
            "value": "test"
        },
        "some_string": {
            "type": "string",
            "required": true,
            "max_length": 5,
            "value": "585"
        },
        "a_datetime": {
            "required": true,
            "type": "datetime",
            "value": "2012-01-18T11:45:00+01:00"
        },
        "a_time": {
            "required": true,
            "type": "time",
            "value": "18:30:00"
        },
        "a_date": {
            "required": true,
            "type": "date",
            "value": "1812-01-18"
        },
        "a_boolean": {
            "required": true,
            "type": "boolean",
            "value": true
        },
        "some_boolean": {
            "type": "bool_as_int",
            "value": 1
        },
        "some_flat_array": {
            "required": false,
            "type": "array_flat",
            "value_type": "int",
            "value": [
                0,
                0,
                3,
                4
            ]
        },
        "some_array": {
            "type": "associative_array",
            "required": true,
            "types": {
                "enabled": {
                    "type": "boolean",
                    "default": true
                },
                "enable_on": {
                    "type": "datetime"
                },
                "name": {
                    "type": "string"
                },
                "id": {
                    "type": "integer",
                    "required": true
                },
                "sub_array": {
                    "required": true,
                    "type": "associative_array",
                    "types": {
                        "name": {
                            "type": "string"
                        },
                        "id": {
                            "type": "integer",
                            "required": true
                        }
                    }
                }
            },
            "value": [
                {
                    "enabled": true,
                    "name": "name",
                    "id": 55,
                    "sub_array": [
                        {
                            "name": "name",
                            "id": 58
                        }
                    ]
                }
            ]
        },
        "some_boolean_with_default": {
            "type": "boolean",
            "default": false,
            "value": false
        },
        "some_int": {
            "type": "integer",
            "default": 5,
            "value": 5
        },
        "a_list": {
            "list_data_type": "int",
            "type": "list",
            "value": [
                1,
                2,
                3
            ]
        }
    }
}		
		
		
	*/
	
	
?>