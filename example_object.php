<?php
	
	include("helpers.php");
	include("Validator.php");

/*
	example payload 
	
{
    "an_object":
        {
            "string_value":1,
            "an_array":[
                {
                    "UUID":"UUID",
                    "comment":"comment",
                    "name":"nameame",
                    "value":"value"
                }
            ]
        }
    
}	
	
*/
	
    $params = [
		
		'an_object'=>[
		    'required'=>false,
		    'type'=>'object',
		    'properties'=>[
		        'string_value'=>[
		            'type'=>'string',
		            'required'=>true,
		            'ignore_if_null' => true,
		            'max_length'=>2048
		        ],
		        'an_array'=>[
		            'required'=>true,
		            'type'=>'array',
		            'properties'=>[
		                'UUID'=>[
		                    'type'=>'string',
		                    'required'=>true,
		                    'ignore_if_null' => true,
		                ],
		                'comment'=>[
		                    'type'=>'string',
		                    'required'=>true,
		                    'ignore_if_null' => true,
		                    'max_length'=>2048
		                ],
		                'type'=>[
		                    'type'=>'string',
		                    'required'=>true,
		                    'default'=>'text',
		                    'valid_values'=>['text','file']
		                ],
		                'name'=>[
		                    'type'=>'string',
		                    'required'=>true,
		                    'ignore_if_null' => true,
		                    'max_length'=>64
		                ],
		                'value'=>[
		                    'type'=>'string',
		                    'required'=>true,
		                    'ignore_if_null' => true,
		                    'max_length'=>2048
		                ],
		            ],
		        ],
		    ],
		],

    ];
    	
    $validator = new Validator();
    try {
      $validator->setParameters($params);
    } catch (\Exception $e) {
      sendBasicResponseAndDie(false, $e->getMessage(), $e->getCode());
    }    
    
    $validated_parameters = $validator->getValidParameters();
    
    sendResponseAndDie(["success" => true, "localization_code" => 2 , "response" => "Validated parameters returned.", "validated_parameters" => $validated_parameters ]);    
	
?>