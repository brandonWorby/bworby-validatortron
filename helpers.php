<?php
	
	
	function sendResponseAndDie($arr, $response_status = '') {
		$arr = convertArrayValuesToUTF8($arr);
		header('Content-type: application/json');
		
		if( isset( $_REQUEST['callback'] ) ) {
			echo( strlen($_REQUEST['callback']) > 0 ) ? $_REQUEST['callback'] . "(" . json_encode($arr) . ")" : json_encode($arr);			
		} else {
			echo json_encode($arr);		
		}
	
		if (json_last_error() != JSON_ERROR_NONE) { 
			error_log("json_last_error: " . json_last_error() . "     array: " . print_r($arr, true)); //if json_encode fails log it	
		}
		die();
	}
	
	//this function accepts a success and response value and then calls sendResponseAndDie.  It is useful for error handling that requires a JSON/XML response.
	function sendBasicResponseAndDie($success, $response, $localization_code, $response_status = '') {
		$success = (bool)$success;
		$localization_code = (int)$localization_code;
		$arr = array();
		$arr['success'] = $success;
		$arr['response'] = $response;
		$arr['localization_code'] = $localization_code;
		sendResponseAndDie($arr, $response_status);
	}
	
	function convertArrayValuesToUTF8(array $arr) {
		if (is_array($arr)) {
			foreach ($arr as $key => &$val) {
				if (is_array($val)) {
					$val = convertArrayValuesToUTF8($val);
				} else {
					if (!mb_check_encoding($val, 'UTF-8')) {
						$val = utf8_encode($val);
					}
				}
			}
		}
		return $arr;
	}
	
	
	
	function forceType( $val , $type ){
		
		switch ( strtolower( $type ) ) {
		case 'bool':
		case 'boolean':
			return boolval($val);
		break;
		case 'bool_as_int':
		case 'boolean_as_int':
			if( !is_null( $val ) ){
				if( $val == 1 ){
					return 1;
				} else {
					return boolToInt($val);
				}
			} else {
				return null;
			}
		break;
		case 'float':
			return (float) $val;
		break;
		case 'integer':
		case 'int':
			return (int) $val;
		break;
		case 'str':
		case 'string':
			return trim((string) $val);
		break;
		case 'date':
			return (string) $val;
		break;
		case 'time':
			return (string) $val;
		break;
		case 'datetime':
			return (string) $val;
		break;
		default:
			return $val;
		}
	}
	
	function boolToInt($bool) {
		if( is_string( $bool ) ){
			$bool = $bool === 'true'? true: false;
		}
		if( $bool != false ) {
			return (int)1;
		} else {
			return (int)0;
		}
	}
	
	//this method verifies that an email address is compliant with RFC822 specs
	function isValidEmail($email_address) {
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		}
		return false;
	}  

?>